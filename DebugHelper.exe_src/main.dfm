object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Little Debug Helper'
  ClientHeight = 382
  ClientWidth = 559
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    559
    382)
  PixelsPerInch = 96
  TextHeight = 13
  object btnClear: TButton
    Left = 8
    Top = 332
    Width = 73
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Clear'
    TabOrder = 0
    OnClick = btnClearClick
  end
  object sbStatus: TStatusBar
    Left = 0
    Top = 363
    Width = 559
    Height = 19
    Panels = <
      item
        Width = 100
      end
      item
        Width = 50
      end>
  end
  object btnSave: TButton
    Left = 167
    Top = 332
    Width = 96
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Save as text'
    TabOrder = 2
    OnClick = btnSaveClick
  end
  object cbOnTop: TCheckBox
    Left = 375
    Top = 340
    Width = 162
    Height = 17
    Anchors = [akLeft, akBottom]
    Caption = 'Always on the top'
    TabOrder = 3
  end
  object rteCil: TRichEdit
    Left = 0
    Top = 0
    Width = 559
    Height = 326
    Align = alTop
    Anchors = [akLeft, akTop, akRight, akBottom]
    Color = clWhite
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object btnSaveRTF: TButton
    Left = 269
    Top = 332
    Width = 100
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'Save as RTF'
    TabOrder = 5
    OnClick = btnSaveRTFClick
  end
  object btnUpload: TButton
    Left = 87
    Top = 332
    Width = 74
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = 'WWW link'
    TabOrder = 6
    OnClick = btnUploadClick
  end
  object tmrUpdate: TTimer
    Interval = 500
    OnTimer = tmrUpdateTimer
    Left = 24
    Top = 24
  end
  object saveText: TSaveDialog
    Filter = 'TXT file|*.txt|LOG file|*.log'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 80
    Top = 24
  end
  object saveRTF: TSaveDialog
    DefaultExt = 'rtf'
    Filter = 'RTF file|*.rtf'
    Options = [ofHideReadOnly, ofExtensionDifferent, ofPathMustExist, ofEnableSizing]
    Left = 136
    Top = 24
  end
end

UNIT main;

INTERFACE

USES
  gnugettext,
  ExtCtrls,
  ComCtrls,
  StdCtrls,
  Dialogs,
  Classes,
  Controls,
  Forms,
  Windows,
  SysUtils,
  synacode,
  httpsend,
  synautil, Graphics;

CONST
  cInfo = 100;


VAR
  pocet  : Longint;

TYPE
  TForm1 = CLASS (TForm )
    btnClear  : TButton;
    tmrUpdate  : TTimer;
    sbStatus  : TStatusBar;
    btnSave  : TButton;
    saveText  : TSaveDialog;
    cbOnTop  : TCheckBox;
    rteCil  : TRichEdit;
    btnSaveRTF  : TButton;
    saveRTF  : TSaveDialog;
    btnUpload  : TButton;
    PROCEDURE PrijemZpravy(VAR Msg  : TMsg; VAR Handled  : Boolean );
    PROCEDURE PridejUdalost(Text  : String );
    PROCEDURE FormCreate(Sender  : TObject );
    PROCEDURE tmrUpdateTimer(Sender  : TObject );
    PROCEDURE btnClearClick(Sender  : TObject );
    PROCEDURE btnSaveClick(Sender  : TObject );
    PROCEDURE btnSaveRTFClick(Sender  : TObject );
    PROCEDURE btnUploadClick(Sender  : TObject );
  Private
    { Private declarations }
  Public
    { Public declarations }
  END;

VAR
  Form1  : TForm1;

IMPLEMENTATION

VAR
  joined  : String;
  joining  : Boolean;

{$R *.dfm}

FUNCTION GetUserFromWindows  : String;
VAR
  UserName  : String;
  UserNameLen  : Dword;
BEGIN
  UserNameLen := 255;
  SetLength(userName, UserNameLen );
  IF GetUserName(Pchar(UserName ), UserNameLen ) THEN
    Result := Copy(UserName, 1, UserNameLen - 1 )
  ELSE
    Result := 'Unknown';
END;


PROCEDURE TForm1.PrijemZpravy(VAR Msg  : TMsg; VAR Handled  : Boolean );
VAR
  Zprava  : Pchar;
BEGIN
  IF Msg.message = 1024 + cInfo THEN
  BEGIN
    Zprava := StrAlloc(Msg.wParam + 1 );
    GlobalGetAtomName(Msg.lParam, Zprava, Msg.wParam + 1 );
    GlobalDeleteAtom(Msg.lparam );

    //pokud nespojuju ani nic jin�ho ned�l�m
    IF (not joining ) and (Zprava <> '[parted]' ) THEN
    BEGIN
      PridejUdalost(Zprava );
      StrDispose(Zprava );
      Handled := True;
      exit;
    END;
    //konec spojov�n�
    IF Zprava = '[/parted]' THEN
    BEGIN
      joining := False;
      PridejUdalost(joined );
      joined := '';
    END;
    //pr�b�h spojov�n�
    IF joining and (Zprava <> '[/parted]' ) THEN
    BEGIN
      joined := joined + String(Zprava );
      StrDispose(Zprava );
    END;
    //zah�jen� spojov�n�
    IF Zprava = '[parted]' THEN
    BEGIN
      joined := '';
      joining := True;
    END;
    Handled := True;
  END;
END;


PROCEDURE TForm1.tmrUpdateTimer(Sender  : TObject );
BEGIN
  sbStatus.Panels[0].Text := Format(_('%d records' ), [pocet] );
  IF cbOnTop.Checked THEN
    Form1.FormStyle := fsStayOnTop
  ELSE
    Form1.FormStyle := fsNormal;
END;


PROCEDURE TForm1.PridejUdalost(Text  : String );
VAR
  txt  : String;
BEGIN
  txt := Copy(Text, 1, 3 );
  Delete(Text, 1, 3 );
  IF txt = '[/p' THEN
    exit;
  rteCil.SelAttributes.Color := 0;
  IF txt = '[E]' THEN
    rteCil.SelAttributes.Color := 255;
  IF txt = '[W]' THEN
    rteCil.SelAttributes.Color := 36095;
  IF txt = '[P]' THEN
    rteCil.SelAttributes.Color := 16711680;
  rteCil.Lines.Add(txt + ' ' + Text );
  rteCil.SetFocus;
  rteCil.SelStart := rteCil.GetTextLen;
  inc(pocet );
END;


PROCEDURE TForm1.btnClearClick(Sender  : TObject );
BEGIN
  rteCil.Lines.Clear;
  pocet := 0;
END;


PROCEDURE TForm1.btnSaveClick(Sender  : TObject );
VAR
  ext  : String;
  fn  :  String;
BEGIN
  IF saveText.Execute THEN
    TRY
      CASE saveText.FilterIndex OF
        1  :
          ext := '.txt';
        2  :
          ext := '.log';
      END;
      fn := ChangeFileExt(saveText.FileName, ext );
      rteCil.PlainText := True;
      rteCil.Lines.SaveToFile(fn );
      rteCil.PlainText := False;
    EXCEPT
      rteCil.PlainText := False;
      ShowMessage(_('Error while saving to file:' ) + #13#10 + fn );
    END;
END;


PROCEDURE TForm1.btnSaveRTFClick(Sender  : TObject );
VAR
  ext  : String;
  fn  :  String;
BEGIN
  IF saveRTF.Execute THEN
    TRY
      ext := '.rtf';
      fn  := ChangeFileExt(saveRTF.FileName, ext );
      rteCil.Lines.SaveToFile(fn );
    EXCEPT
      ShowMessage(_('Error while saving to RTF:' ) + #13#10 + fn );
    END;
END;


PROCEDURE TForm1.btnUploadClick(Sender  : TObject );
VAR
  http  : THttpSend;
  Data  : TMemoryStream;
  size  : Integer;
  name  : String;
  I  : Integer;
  Result  : Boolean;
  URL  : String;
BEGIN
  name := InputBox(_('Your Name' ), _('Please Enter your name' ), GetUserFromWindows );
  screen.Cursor := crHourGlass;
  sbStatus.Panels[1].Text := _('Uploading...' );

  rteCil.PlainText := True;
  Data := TMemoryStream.Create;
  rteCil.Lines.SaveToStream(Data );
  rteCil.PlainText := False;

  Data.Position := 0;
  size := Length(System.AnsiToUtf8(ReadStrFromStream(Data, Data.Size ) ) );

  Data.Position := 0;

  HTTP := THTTPSend.Create;
  TRY
    WriteStrToStream(HTTP.Document, 'author=' + EncodeURLElement(AnsiToUtf8(name ) ) +
      '&pasteEnter=' + EncodeURLElement(AnsiToUtf8(ReadStrFromStream(Data, Data.Size ) ) ) );
    HTTP.MimeType := 'application/x-www-form-urlencoded';
    Result := HTTP.HTTPMethod('POST', 'http://p.mzf.cz/api' );
    //error handling
    IF not (Result and (Pos('Success!', 'SSS' +
      ReadStrFromStream(http.Document, http.Document.Size ) ) > 3 ) ) THEN
    BEGIN
      IF size <= 10 THEN
        ShowMessage(_('The minimal upload size was not reached!' ) )
      ELSE
      IF size >= 512 * 1024 THEN
        ShowMessage('You reached maximum upload size! (~500k)' )
      ELSE
        ShowMessage('Unknown Error!' );
    END
    ELSE
    BEGIN
      //URL
      HTTP.Document.Position := 0;
      URL := ReadStrFromStream(http.Document, http.Document.Size );
      URL := SeparateRight(URL, '"url"' );
      URL := SeparateRight(URL, '"' );
      URL := SeparateLeft(URL, '"' );
      InputBox(_('Your link:' ), _('Now you can copy the link!' ), URL );
    END;
  FINALLY
    HTTP.Free;
    Data.Free;
  END;
  sbStatus.Panels[1].Text := '';
  screen.Cursor := crDefault;
END;


PROCEDURE TForm1.FormCreate(Sender  : TObject );
BEGIN
  textdomain('default');
  TranslateComponent(Self );
  joined := '';
  joining := False;
  Application.OnMessage := PrijemZpravy;
  pocet  := 0;
END;

END.


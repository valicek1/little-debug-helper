{$I DebugHelper.lib.pas}


{$I DebugHelper.version.inc}

function PluginCaption: string;
begin
  Result := 'Test Debuggeru' + plgman_plugin_version;
end;

function PluginHint: string;
begin
  Result := 'Otestuje debugger';
end;

function PluginIcon: string;
begin
  Result := '';
end;

function PluginFlags: string;
begin
  Result := 'nowork';
end;

procedure PluginStart;
begin
  LDHInit(true);
  LDHTest(1000);
  LDHD('Dlouh� �et�zce:', ldhError);
  LDH('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed dui ante, vulputate ut dapibus non, luctus at erat. Aenean ultrices euismod ultricies. Fusce quis turpis metus. Proin vitae nisi ante. In hac habitasse platea dictum ); //st. Maecenas lorem tortor, tristique sit amet fringilla ac, iaculis quis elit. Curabitur cursus pharetra aliquet. Curabitur molestie, lacus elementum adipiscing fringilla, diam eros tincidunt nisl, vitae volutpat nisl augue sed erat. Aliquam erat volutpat. Aliquam erat volutpat. Pellentesque laoreet, ligula rutrum tristique varius, nulla mi fermentum arcu, at accumsan velit erat eget est. Suspendisse potenti. Suspendisse fermentum blandit porttitor. '
   + 'Morbi lobortis, turpis vitae molestie posuere, felis felis rutrum nisl, id dictum eros velit quis mauris. Sed viverra sollicitudin iaculis. Quisque nulla lectus, posuere sed mollis et, blandit ac tellus. Proin ultrices posuere tincidunt. Maecenas iaculis tempor tortor eget suscipit. Etiam diam quam, tempor at adipiscing sed, pellentesque eu nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent posuere mi quis dolor rhoncus placerat at non leo. '
   + 'Donec sit amet metus ac quam hendrerit ultricies sed id quam. Nullam posuere pulvinar dui, at sollicitudin leo egestas nec. Vivamus consectetur est ac neque consectetur interdum. Aliquam interdum, metus et placerat faucibus, velit justo posuere quam, eget fringilla neque justo a lectus. Nullam condimentum, dui sit amet semper fringilla, dui lorem gravida dui, vel ornare nisi risus a nisi. Vivamus porta consectetur purus, eu tempus massa auctor placerat. Donec ut libero dolor. In neque leo, eleifend sed consequat a, dapibus at velit. Pellentesque sed ante non diam scelerisque dictum. Donec enim massa, tincidunt at congue et, pellentesque eget felis. In interdum vehicula nisi vel ornare. Aliquam neque velit, blandit quis consequat nec, ultricies in felis. Proin eget mollis diam. Vivamus non nibh sed dolor viverra suscipit eget a tortor. '
   + 'Suspendisse vestibulum metus in velit imperdiet vitae adipiscing magna dignissim. Proin et erat tortor. Aliquam dapibus feugiat facilisis. Maecenas consectetur eleifend metus et ullamcorper. Curabitur sed est vel magna sodales suscipit ac et nisi. Quisque venenatis sagittis consectetur. Quisque dignissim dui a nisi volutpat mollis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. '
   + 'Vestibulum hendrerit lacinia diam quis tincidunt. Praesent at metus ac erat placerat blandit. Vestibulum eget commodo dolor. Sed ullamcorper sagittis convallis. Praesent tempus purus et lorem interdum malesuada. Etiam molestie, neque non cursus facilisis, massa libero pretium odio, eu elementum nunc nisi ac odio. Ut tincidunt condimentum tortor, vitae adipiscing est posuere et. Cras ligula mauris, sodales vel convallis quis, luctus at urna. Aenean consequat, odio non accumsan molestie, turpis lorem blandit mauris, sodales hendrerit orci purus non massa. Duis risus velit, dictum sit amet elementum sit amet, facilisis et lacus. Proin a nunc magna. Etiam eu nibh ut nunc imperdiet pulvinar et eu nulla. Suspendisse potenti. '
   + 'Pellentesque nec sem arcu. Donec tincidunt lacinia ante, ut egestas nisl tempor non. Ut pretium vestibulum posuere. Mauris aliquam venenatis lorem et pellentesque. Phasellus nibh justo, fringilla nec iaculis a, porta sit amet eros. Maecenas porta, mauris quis pulvinar lobortis, odio elit congue nisl, id ornare tellus lectus eget arcu. Suspendisse dui ipsum, hendrerit lacinia convallis sit amet, lacinia adipiscing justo. Praesent ac consectetur ante. Nullam nec turpis nunc, ut viverra erat. Fusce in nunc at enim sodales lobortis vel at neque. '
   + 'Pellentesque id leo volutpat velit luctus suscipit ut mollis risus. Praesent sed ante nibh, vitae consectetur tortor. Suspendisse iaculis odio quis metus vulputate hendrerit. Curabitur et leo ut lorem scelerisque vestibulum. Maecenas non hendrerit libero. Donec leo nisl, eleifend non vulputate viverra, vulputate nec velit. Nullam placerat quam ligula, quis semper nisl. '
   + 'In sit amet nibh nisi. Nunc odio nisi, facilisis non feugiat ornare, lobortis vel elit. Donec fringilla, sem at consectetur accumsan, diam sem blandit risus, vel luctus eros urna eu nunc. Donec malesuada ligula non tortor tempus volutpat. Integer et tellus vitae velit blandit facilisis sed in tellus. Phasellus lacinia urna a leo volutpat et luctus nibh consequat. Vivamus velit ligula, vestibulum quis sodales a, elementum sit amet ligula. In scelerisque leo id odio luctus pulvinar. Ut rhoncus, enim ac rhoncus aliquam, sem tellus aliquam arcu, at aliquam ligula nisl at tortor. Ut quis augue in tortor placerat tristique. Vestibulum molestie purus nec purus vulputate volutpat. Curabitur tincidunt mauris accumsan mauris porttitor a volutpat tellus pellentesque. '
   + 'Suspendisse quis sollicitudin tellus. Ut tincidunt, metus sed dapibus ultricies, nibh neque congue lectus, eget feugiat libero nulla sit amet augue. Donec diam nisl, imperdiet sit amet adipiscing metus.');
end;
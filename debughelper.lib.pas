// Knihovna pro snadn�j�� lad�n� plugin� v GeoGetu
// V podstat� se jedn� o n�hradu ShowMessage()
// Nav�c je v�stup p�esm�rovan� do samostatn� aplikace, tak�e p�i p�du GG m�m logy st�le k dispozici
// Aplikace se m��e "ucpat" v p��pad�, �e j� p�ed�m r�dov� tis�ce log� zar�z

const
   LDH_WM_USER = 1024; //Z hex: 0x0400
   LDHcDebug  = 100;
   LDHApp = '\script\lib\DebugHelper\DebugHelper.exe';
   
type TLDHReportType = (
   ldhInfo,        //Informace, �ern� barva
   ldhError,       //Chyba, �erven� barva
   ldhWarning,     //Varov�n�, �lut� barva
   ldhProcedure);  //Procedury, modr� barva
   
var
   LDH_WND     : longint; //Handle okna debuggeru, nap��klad pro obsluhu pomoc� user32.dll
   LDH_Enabled : boolean;
  

//------------------------------------------------------------------------------
{Inicializace funkc� z DLL knihoven}
//vr�t� m� handle okna s dan�m titulkem
function LDHFindWindow(C1, C2: PChar): Longint; external 'FindWindowA@user32.dll stdcall';
//donut� okno s dan�m handle uk�zat se
function LDHShowWindow(hWnd, nCmdShow: Longint): Integer; external 'ShowWindow@user32.dll stdcall';
//p�ed� zpr�vu oknu s handle hwnd
function LDHPostMessage(hWnd:HWND; Msg:LongInt; wParam: word; lParam:integer):Longint; external 'PostMessageA@user32.dll stdcall';
//p�id� zpr�vu, vr�t� jej� pointer
function LDHGlobalAddAtom(atomName: Pchar): word; external 'GlobalAddAtomA@kernel32.dll stdcall';


//------------------------------------------------------------------------------
{odesl�n� zpr�vy - intern� funkce, NEPOU��VAT! - p�i odesl�n� textu del��ho ne� 256 znak� dojde k chyb� aplikace!}
Procedure LDH_Send(Text: string);
var
  lParam: word;
begin
  //pokud je vypnut� debugov�n�, pak nic nepos�l�m
  if LDH_Enabled = false then exit;
  //jinak po�lu
  try
    lParam := LDHGlobalAddAtom(PChar(Text));
    LDHPostMessage(LDH_WND, LDH_WM_USER + LDHcDebug, Length(Text), lParam);
  finally
    //GlobalDeleteAtom(lParam); - toto d�l� loguj�c� aplikace
  end;
end;


//------------------------------------------------------------------------------
{Odesl�n� zpr�vy}
Procedure LDHD(text: string; kind: TLDHReportType);
var
  prefix: string;
  part : string;
begin
  case kind of
    ldhInfo      : prefix:='[I]';
    ldhError     : prefix:='[E]';
    ldhWarning   : prefix:='[W]';
    ldhProcedure : prefix:='[P]';
  end;
  text := Prefix + ' [' + FormatDateTime('hh:mm:ss.zzz', NOW) + '] ' + text;
  //do atomu se vejde maxim�ln� 256 bajt�
  if Length(text) >= 254 then
  begin
    LDH_Send('[parted]');
    while Length(text) >= 254 do
    begin
      part := Copy(Text, 1, 254);
      Delete(Text, 1, 254);
      LDH_Send(part);
    end;
    LDH_Send(text);
    LDH_Send('[/parted]');
  end else LDH_Send(text);
  sleep(4);
end;

//-----------------------------------------------------------------------------
{Odesl�n� Info zpr�vy - zkr�cen�}
procedure LDH(Text: string);
begin
  LDHD(Text, ldhInfo);
end;

//-----------------------------------------------------------------------------
{Odesl�n� Warning zpr�vy - zkr�cen�}
procedure LDHw(Text: string);
begin
  LDHD(Text, ldhWarning);
end;

//-----------------------------------------------------------------------------
{Odesl�n� Procedure zpr�vy - zkr�cen�}
procedure LDHp(Text: string);
begin
  LDHD(Text, ldhProcedure);
end;

//-----------------------------------------------------------------------------
{Odesl�n� Error zpr�vy - zkr�cen�}
procedure LDHe(Text: string);
begin
  LDHD(Text, ldhError);
end;

//------------------------------------------------------------------------------
{test debuggeru}
procedure LDHTest(count: integer);
var i: integer;
begin
  for i:=0 to count-1 do LDHD(_('Little debug helper test') + ' ' + IntToStr(i), TLDHReportType(i mod 4));
end;

//------------------------------------------------------------------------------
{Inicializace debuggeru}
procedure LDHInit(enabled: boolean);
begin
  if not enabled then
  begin
    LDH_Enabled:=Enabled;
    exit;
  end;
  LDH_Enabled:=Enabled;
  LDH_WND := LDHFindwindow('', 'Little Debug Helper');
  //pokud aplikace neb��
  if LDH_WND = 0 then
  begin
    RunExecNoWait(GEOGET_DATADIR + LDHApp);
    //D�me aplikaci �anci se zapnout
    sleep(2000);
    LDH_WND := LDHFindwindow('', 'Little Debug Helper');
  end;
  //pokud se i p�es to nepovedlo otev��t Aplikaci
  if LDH_WND = 0 then
  begin
    //vypneme lad�n�
    LDH_Enabled := false;
    exit;
  end;
  //uk�u okno.. Aby vysko�ilo, mus�m je nejd��v schovat
  LDHShowWindow(LDH_WND, 0);
  LDHShowWindow(LDH_WND, 5);
  LDHD(_('Debug Helper connected...'), ldhInfo);
end;